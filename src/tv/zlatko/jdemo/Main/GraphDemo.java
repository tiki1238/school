/**
 * Created on 28.11.17
 * Copyright 2015 Cellent Finance Solutions GmbH
 * Calwer Strasse 33
 * 70173 Stuttgart
 * www.cellent-fs.de
 */
package tv.zlatko.jdemo.Main;

import javafx.util.Pair;

import java.util.*;

public class GraphDemo {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);

        final int V = 5;
        final int n = 6;

        final List<Pair<Integer, Integer>>[] graph = new List[V];

        for (int i = 0; i < n; i++) {
            System.out.println("Enter start vertex for node " +i+ " :");
            final int v1 = scanner.nextInt();
            System.out.println("Enter end vertex for node " +i+ " :");
            final int v2 = scanner.nextInt();
            System.out.println("Enter weight for node " +i+ " :");
            final int w = scanner.nextInt();
            final int vertexIndex1 = v1 - 1;
            if (graph[vertexIndex1] == null) {
                graph[vertexIndex1] = new LinkedList<>();
            }
            graph[vertexIndex1].add(new Pair<>(v2, w));
            final int vertexIndex2 = v2 - 1;
            if (graph[vertexIndex2] == null) {
                graph[vertexIndex2] = new LinkedList<>();
            }
            graph[vertexIndex2].add(new Pair<>(v1, w));
        }

        print(graph);
    }

    public static void print(List<Pair<Integer, Integer>>[] graph) {
        for (int i = 0; i < graph.length; i ++) {
            if (graph[i] == null) {
                System.out.println("No data for vertex " + i);
            } else {
                for (int j = 0; j < graph[i].size(); j++) {
                    Pair<Integer, Integer> data = graph[i].get(j);
                    System.out.println("Node from " + (i+1) + " to " + data.getKey() + " = " + data.getValue());
                }
            }
        }
    }
}
