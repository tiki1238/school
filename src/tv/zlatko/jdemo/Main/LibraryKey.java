/**
 * Created on 28.11.17
 * Copyright 2015 Cellent Finance Solutions GmbH
 * Calwer Strasse 33
 * 70173 Stuttgart
 * www.cellent-fs.de
 */
package tv.zlatko.jdemo.Main;

public class LibraryKey {


    String author;
    String title;
    int year;

    private LibraryKey(final String author, final String title, final int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }

    @Override
    public int hashCode() {
        return author.hashCode() + title.hashCode() + year;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof LibraryKey) {
            LibraryKey target = (LibraryKey) obj;
            return author.equals(target.author) && title.equals(target.title) && year == target.year;
        } else {
            return false;
        }
    }
}
