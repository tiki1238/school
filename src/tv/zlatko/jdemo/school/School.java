package tv.zlatko.jdemo.school;

import tv.zlatko.jdemo.school.DataStorage.Reader;
import tv.zlatko.jdemo.school.DataStorage.StudentReaderException;
import tv.zlatko.jdemo.school.klas.Klas;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class School {

    private List<SchoolListener> listeners = new LinkedList<>();

    private List<Klas> klasses = new ArrayList<>();

    public List<Klas> getKlasses() {
        return Collections.unmodifiableList(klasses);
    }

    public void addListener(SchoolListener l) {
        listeners.add(l);
    }

    public void removeListener(SchoolListener l) {
        listeners.remove(l);
    }

    public void addKlas(String name) {
        klasses.add(new Klas(name));
        fireChanged();
    }

    private void fireChanged() {
        for (SchoolListener sl : listeners) {
            sl.klassesChanged(this);
        }
    }

    public void addKlas(Klas k) {
        klasses.add(k);
        fireChanged();
    }

    public void deleteKlas(int klasIndex) {
        klasses.remove(klasIndex);
        fireChanged();
    }

    public void clear() {
        klasses.clear();
        fireChanged();
    }
}

