package tv.zlatko.jdemo.school.klas;

import tv.zlatko.jdemo.school.DataStorage.Reader;
import tv.zlatko.jdemo.school.DataStorage.StudentReaderException;
import tv.zlatko.jdemo.school.DataStorage.Writer;
import tv.zlatko.jdemo.school.School;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by zlatko on 09.02.18.
 */
public class SchoolController {
    private final School school;

    public SchoolController(final School school) {
        this.school = school;
    }

    public void read() throws StudentReaderException {
        final File file = new File("data.txt");
        if (file.exists()) {
            Reader reader = new Reader(file);
            final Collection<Klas> klasses = reader.getClasses();
            school.clear();
            for (Klas klass : klasses) {
                school.addKlas(klass);
            }
        }
    }

    public void save() throws IOException {
        Writer writer = new Writer("data.txt");
        for (Klas klas : school.getKlasses()) {
            writer.writeData(klas);
        }

        writer.close();
    }
}
