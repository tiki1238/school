package tv.zlatko.jdemo.school.DataStorage;

import tv.zlatko.jdemo.school.Student;
import tv.zlatko.jdemo.school.klas.Klas;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Victor on 01.02.18.
 */

public class Reader {
    private List<Klas> klasses = new LinkedList<>();

    public Reader(File file) throws StudentReaderException {
        try {
           Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                    readOneKlas(scanner);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new StudentReaderException();
        }
    }

    private void readOneKlas(Scanner scanner) throws StudentReaderException {
        final String name = scanner.nextLine();
        if(name.equals("***")) return;
        final Klas klas = new Klas(name);
        klasses.add(klas);
        while (scanner.hasNextLine()) {
            String string = scanner.nextLine();
            if ("***".equals(string)) {
                break;
            }
            String[] data = string.split(" ");
            DateFormat format = new SimpleDateFormat("MMM dd HH:mm:ss yyyy");
            if(data.length==8) {
                String dateTxt = data[3] + " " + data[4] + " " + data[5] + " " + data[7];
                try {
                    Date date = format.parse(dateTxt);
                    klas.addStudent(new Student(data[0] + " " + data[1], date));
                } catch (ParseException ex) {
                    throw new StudentReaderException("Error parsing date!");
                }
            }else{
                String dateTxt = data[2] + " " + data[3] + " " + data[4] + " " + data[6];
                try {
                    Date date = format.parse(dateTxt);
                    klas.addStudent(new Student(data[0], date));
                } catch (ParseException ex) {
                    throw new StudentReaderException("Error parsing date!");
                }
            }
        }
    }
    public Collection<Klas> getClasses() {return Collections.unmodifiableCollection(klasses);}
}

