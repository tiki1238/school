package tv.zlatko.jdemo.school.DataStorage;

import tv.zlatko.jdemo.school.Student;
import tv.zlatko.jdemo.school.klas.Klas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Victor on 01.02.18.
 */

public class Writer {

    private BufferedWriter target;

    public Writer(String file) throws IOException {
        this.target = new BufferedWriter(new FileWriter(file));
    }


    public void writeData(Klas klas) throws IOException {
        target.write(klas.getName());
        target.newLine();
        for (Student s : klas.getStudents()) {
            target.write(s.getName() + " " + s.getDob());
            target.newLine();
        }
        target.write("***");
        target.newLine();
    }

    public void close() throws IOException {
        target.close();
    }
}

