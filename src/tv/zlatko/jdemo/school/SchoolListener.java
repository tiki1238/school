package tv.zlatko.jdemo.school;

public interface SchoolListener {
    void klassesChanged(School s);
}
