package tv.zlatko.jdemo.school;

import tv.zlatko.jdemo.school.DataStorage.StudentReaderException;
import tv.zlatko.jdemo.school.klas.Klas;
import tv.zlatko.jdemo.school.klas.KlasModelController;
import tv.zlatko.jdemo.school.klas.KlasTableModel;
import tv.zlatko.jdemo.school.klas.SchoolController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;

public class App {

    private static final int GAP = 10;
    private static int selectedIndex;

    private static void updateData(SchoolController schoolController, School school,
                                   JFrame frame, JComboBox comboBox) {
        try {
            schoolController.save();
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(frame, e1.getMessage(), "Error when saving data", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String[] args) {
        final School school = new School();

        // UI
        final JFrame frame = new JFrame("Some title");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 800, 500);
        frame.setLayout(new BorderLayout(GAP, GAP));


        final JPanel panelTop = new JPanel(new BorderLayout(GAP, GAP));
        frame.add(panelTop, BorderLayout.NORTH);
        final KlasTableModel dataModel = new KlasTableModel();
        final JTable table = new JTable(dataModel);
        frame.add(new JScrollPane(table), BorderLayout.CENTER);
        final JPanel panelButtons = new JPanel(new GridLayout(10, 1, GAP, GAP));
        frame.add(panelButtons, BorderLayout.EAST);

        panelTop.setBorder(new EmptyBorder(GAP, 0, 0, 0));
        panelTop.add(new JLabel("Клас:"), BorderLayout.WEST);
        final JComboBox comboBox = new JComboBox();
        comboBox.setModel(new AddClassComboModel(school));
        panelTop.add(comboBox, BorderLayout.CENTER);
        final KlasModelController controller = new KlasModelController(dataModel);
        final SchoolController schoolController = new SchoolController(school);

        panelButtons.add(new JButton(new AbstractAction("+") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if(school.getKlasses().size()==0) {
                    JOptionPane.showMessageDialog(frame, "Klas not found!");
                    return;
                }
                final String s = JOptionPane.showInputDialog(frame, "Enter student name!");
                if (s == null || s.length() == 0) {
                    // user cancel
                    return;
                }
                controller.addNewStudent(s);
                try {
                    schoolController.save();
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage(), "Error when saving data", JOptionPane.ERROR_MESSAGE);
                }
            }
        }));

        panelButtons.add(new JButton(new AbstractAction("-") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int selectedRow = table.getSelectedRow();
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(panelButtons, "Please select a student!");
                } else {
                    controller.removeStudent(selectedRow);
                    try {
                        schoolController.save();
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(frame, e1.getMessage(), "Error when saving data", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }));

        panelButtons.add(new JButton(new AbstractAction("Rename") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                final int selectedRow = table.getSelectedRow();
                if (selectedRow == -1) {
                    JOptionPane.showMessageDialog(panelButtons, "Please select a student!");
                } else {
                    final String s = JOptionPane.showInputDialog(frame, "Enter new name!", dataModel.getStudentAtRow(selectedRow).getName());
                    if (s == null || s.length() == 0) {
                        // user cancel
                        return;
                    } else {
                        controller.renameStudent(s, selectedRow);
                        try {
                            schoolController.save();
                        } catch (IOException e1) {
                            JOptionPane.showMessageDialog(frame, e1.getMessage(), "Error when saving data", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }
            }
        }));
        //Listens for item selection
        comboBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                System.out.println("Item event");
                if ((e.getStateChange() != ItemEvent.SELECTED)||school.getKlasses().size()==0) {
                    return;
                }
                System.out.println("Item event selected");

                if (comboBox.getSelectedIndex() <= school.getKlasses().size() - 1)
                    selectedIndex = comboBox.getSelectedIndex();
                if (selectedIndex == -1) {
                    dataModel.setData(null);
                } else {
                    dataModel.setData(school.getKlasses().get(selectedIndex));
                }
            }
        });
            //Listens for add class button
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final int lastIndex = comboBox.getItemCount() - 1;
                if (comboBox.getSelectedIndex() == lastIndex) {
                    final String s = JOptionPane.showInputDialog(frame, "Enter class name!");
                    if (s == null || s.length() == 0) {
                        // user cancel

                        comboBox.setSelectedIndex(selectedIndex);
                        return;
                    }
                    school.addKlas(s);
                    updateData(schoolController, school, frame, comboBox);
                    comboBox.setSelectedIndex(lastIndex);
                }
            }
        });

        panelTop.add(new JButton(new AbstractAction("Delete Klas") {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if(school.getKlasses().size()==0) {
                    JOptionPane.showMessageDialog(frame, "Klas not found!");
                    return;
                }
                final int confirmation = JOptionPane.showConfirmDialog(frame,
                        "Are you sure you want to delete " + comboBox.getSelectedItem() + " klas?",
                        "Delete", JOptionPane.YES_NO_OPTION);
                if (confirmation != 0) {
                    //user cancel
                    return;
                }
                if(selectedIndex==0) dataModel.setData(null);
                school.deleteKlas(selectedIndex);
                updateData(schoolController, school, frame, comboBox);
            }
        }), BorderLayout.EAST);

        frame.setVisible(true);
        try {
            schoolController.read();
        } catch (StudentReaderException e) {
            JOptionPane.showMessageDialog(frame, e.getMessage());
        }

    }

    // TODO make the combo model listen to the school itself, so
    private static class AddClassComboModel extends DefaultComboBoxModel<String>implements SchoolListener {

        private School school;

        public AddClassComboModel(School school) {
            school.addListener(this);
            this.school = school;
            reloadSchoolData();
        }

        private void reloadSchoolData() {
            removeAllElements();
            for (Klas k : school.getKlasses()) {
                addElement(k.getName());
            }
            fireContentsChanged(this, 0, getSize() - 1);
        }

        @Override
        public int getSize() {
            return super.getSize() + 1;
        }

        @Override
        public String getElementAt(int index) {
            if (index == getSize() - 1) {
                return " + add class";
            }
            return super.getElementAt(index);
        }

        @Override
        public void klassesChanged(School s) {
            if (s != this.school)
                return;

            reloadSchoolData();
        }
    }
}

